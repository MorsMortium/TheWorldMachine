# Contents of animation not present in original
- Fixed bottom cog direction
- Music of original game, with fade in
- Camera movement, in 3 degrees, with mouse
- Bottom plates for cogs, so they are not see through
- Pause animations and stop audio (P)
