extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var Paused = false

func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_P:
			if Paused:
				for Index in range(6):
					get_node("RotatePlayer" + str(Index + 1)).play()
				get_node("/root/Spatial/CameraControl/Camera/Music").play()
				get_node("FadeInPlayer").play()
				Paused = false
			else:
				for Index in range(6):
					get_node("RotatePlayer" + str(Index + 1)).stop(false)
				get_node("/root/Spatial/CameraControl/Camera/Music").stop()
				get_node("FadeInPlayer").stop(true)
				Paused = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
