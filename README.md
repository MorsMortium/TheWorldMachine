# Readme

## Description
This is the World Machine animation, from a game called City of Steam.

## Goal
This repos goal is archiving the animation, as well as adding to it.

## Original
- Unity3d web executable
- Firefox ESR 52.9.0 32 bit (released in 2018)
- Unity web player (deprecated in 2015)
- Obtained from archived website on Wayback Machine

## Defense
- The game is defunct, since 2016.
- The project does not intend to cause any monetary gain.
- Media used with the assumption of fair use.

## In progress
- To see what is added, check [ADDED](ADDED.md).
- To see what is missing, check [TODO](TODO.md).
