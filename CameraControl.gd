extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			var CameraNode = get_node("Camera")
			var Translation = CameraNode.get_translation()
			if event.button_index == BUTTON_WHEEL_UP:
				#if 1.2 < Translation[2]:
				Translation[2] -= 0.1
			if event.button_index == BUTTON_WHEEL_DOWN:
				if Translation[2] < 2.45:
					Translation[2] += 0.1
			CameraNode.set_translation(Translation)
	elif event is InputEventMouseMotion:
		var Size = get_viewport().get_visible_rect().size
		var CurrentX = ((event.position[0] / Size[0] * 2) - 1) * 180
		var CurrentY = ((event.position[1] / Size[1] * 2) - 1) * 180
		set("rotation_degrees", Vector3(CurrentY, CurrentX, 0))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
